let Bubble1;

let blink = true;
let blinkTime = 500;

function setup() {
  // put setup code here
  createCanvas(400, 600)
  Bubble1 = new Bubble(width/2, height/2, 100)

  setInterval( ()=> blink = !blink, blinkTime);
}

function draw() {
  // put drawing code here
  background(0)

  if(blink) 
  {
    textSize(20);
    fill(255);
    text("It's a bubble!", 0, 20);
  }
  //else {}

  Bubble1.Draw()
  Bubble1.Jiggle()
  rect(width/2, height/2, 150, 50)
  rect(350, 350, 50, 50);
}