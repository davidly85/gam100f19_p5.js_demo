class Bubble
{
	constructor(x, y, r)
	{
		this.x = x
		this.y = y
		this.radius = r
		this.color = "#50BCDF"
	}
	ChangeColor()
	{
		this.color = "#" + round(random() * 0xffffff).toString(16);
	}

	Draw()
	{
		push();
		if(keyIsPressed) {this.ChangeColor();}
		fill(this.color);
		circle(this.x, this.y, this.radius)
		pop();
	}

	Jiggle()
	{
		this.x += random(-10, 10);
		this.y += random(-10, 10);
	}
}